$(document).ready(function () {
    /*NAV LIST*/
     $('#nav-icon2').click(function () {
        $(this).toggleClass('open');
        $('.menu-header-overlay').toggle('slow');
        $('.open-menu, .header-top-nav__basket_icon, .menu-header').fadeToggle('slow');
        if(window.matchMedia('(max-width: 767px)').matches)    {
                $('.logo, .header-info__phone').fadeToggle('slow');
            }
    });
    $('#nav-icon3').click(function () {
        $(this).toggleClass('open');
        $('.bottom-nav-mobile_close, #nav-icon3 > p, .bottom-nav-mobile__list').fadeToggle('slow');
    });
    /*CARD BTN*/
    $('.js-btn')
        .on('mouseenter', function (e) {
            var parentOffset = $(this).offset(),
                relX = e.pageX - parentOffset.left,
                relY = e.pageY - parentOffset.top;
            $(this).find('span').css({ top: relY, left: relX })
        })
        .on('mouseout', function (e) {
            var parentOffset = $(this).offset(),
                relX = e.pageX - parentOffset.left,
                relY = e.pageY - parentOffset.top;
            $(this).find('span').css({ top: relY, left: relX })
        });

    /*DROPMENU MOBILE*/
    $('.header-top-nav__basket_icon').on('click', function () {
      $(this).find('.header-top-nav__basket_icon-dropmenu').toggleClass('showDropmenu');
    });

  /*DROPMENU FLY*/
    $('.dropdown-menu-toggler a').on('click', function (e) {
      event.preventDefault();
    });
    $('.dropdown-menu-toggler').on('click', function () {
      $(this).find('.header-top-nav__basket_icon-dropmenu').toggleClass('showDropmenu');
    });

    /*NAV LINKS*/
    $('.header-top-nav li').hover(function () {
        var left = $(this).offset().left - $('.header-top-nav').offset().left,
            width = $(this).width();
        $('.nav-active-border').css({
            'margin-left': left,
            'width': width
            })
    },
        function () {
            $('.nav-active-border').css({
                'width': 0
            })
        }
    ).first().mouseenter().mouseleave();


    $('.bottom-nav li').hover(function () {
        var left = $(this).offset().left - $('.bottom-nav').offset().left,
            width = $(this).width();
        $('.nav-active-border-bottom').css({
            'margin-left': left,
            'width': width
        })
    }).eq(1).mouseenter().mouseleave();    
       

    $('.header-top-nav li a').hover(
        function () {
            $('.header-top-nav li a').removeClass('top-nav-active');
            $(this).addClass('top-nav-active');
        }
    );

    $('.bottom-nav li a').hover(
        function () {
            $('.bottom-nav li a').removeClass('bottom-nav-active');
            $(this).addClass('bottom-nav-active');
        }
    );

    /*fly hover*/
    $('.fly-nav li').hover(function () {
        var left = $(this).offset().left - $('.fly-nav').offset().left,
            width = $(this).width();
        $('.nav-active-border').css({
            'margin-left': left,
            'width': width
        })
    },
        function () {
            $('.nav-active-border').css({
                'width': 0
            })
        }
    ).first().mouseenter().mouseleave();
    /*sticky*/
    if (window.matchMedia('(min-width: 1024px)').matches) {
        $(".menu-fly").sticky({ topSpacing: 0 });

        $(function () {
            $(window).scroll(function () {
                var distanceTop = $('.menu-fly').offset().top;
                if ($(window).scrollTop() == distanceTop)
                    $('.menu-fly').css({ 'opacity': '1' });
                else
                    $('.menu-fly').css({ 'opacity': '0' });
            });
        });
    }

     /*mobile arrows card main*/
    $('.ready-sets-arrow-right').click(function(){
        $('.ready-sets__button_active').next('.ready-btn-js').addClass('ready-sets__button_active');
        $('.ready-sets__button_active').prev().removeClass('ready-sets__button_active');
        $('.ready-sets__button_active-second').next('.ready-btn-js').addClass('ready-sets__button_active-second');
        $('.ready-sets__button_active-second').prev().removeClass('ready-sets__button_active-second');
        if ($('.ready-sets__button_active-second').next('.ready-sets-arrow-right')) {
            $('.ready-sets-arrow-right').css("display", "none");
        }

        if ($('.ready-sets__button_active').prev('.ready-btn-js')) {
            $('.ready-sets-arrow-left').css("display", "block");
        }
    });

    $('.ready-sets-arrow-left').click(function () {
        $('.ready-sets__button_active').prev('.ready-btn-js').addClass('ready-sets__button_active');
        $('.ready-sets__button_active').next().removeClass('ready-sets__button_active');
        $('.ready-sets__button_active-second').prev('.ready-btn-js').addClass('ready-sets__button_active-second');
        $('.ready-sets__button_active-second').next().removeClass('ready-sets__button_active-second');
        if ($('.ready-sets__button_active').prev('.ready-sets-arrow-left')) {
            $('.ready-sets-arrow-left').css("display", "none");
        }

        if ($('.ready-sets__button_active').prev('.ready-btn-js')) {
            $('.ready-sets-arrow-right').css("display", "block");
        }
    });

     /*basket-pay__buttons*/
    $(function () {
        $('.basket-pay__button_delivery').click(function (e) {
            e.preventDefault();
            $('.basket-pay__button_delivery').addClass('basket-pay__button_active-left');
            $('.basket-pay__button_pickup').removeClass('basket-pay__button_active-right');
            $('.basket-address__delivery-time, .basket-address__link').css("display", "flex");
            $('.basket-address__point, .basket-address__textarea').css("display", "none");
        });

        $('.basket-pay__button_pickup').click(function (e) {
            e.preventDefault();
            $('.basket-pay__button_pickup').addClass('basket-pay__button_active-right');
            $('.basket-pay__button_delivery').removeClass('basket-pay__button_active-left');
            $('.basket-address__delivery-time').css("display", "none");
            if(window.matchMedia('(min-width: 1024px)').matches)    {
            	$('.basket-address__link').css("display", "none");
                $('.basket-address__textarea').css("display", "flex");
            }
            $('.basket-address__point').css("display", "flex");
        });

        $('.basket-pay__button_cash').click(function (e) {
            e.preventDefault();
            $('.basket-pay__button_cash').addClass('basket-pay__button_active-left');
            $('.basket-pay__button_card').removeClass('basket-pay__button_active-right');
            $('.bascket-pay__surplus').css("display", "flex");
            $('.basket-pay__text').css("display", "none");
        });
        $('.basket-pay__button_card').click(function (e) {
            e.preventDefault();
            $('.basket-pay__button_card').addClass('basket-pay__button_active-right');
            $('.basket-pay__button_cash').removeClass('basket-pay__button_active-left');
            $('.bascket-pay__surplus').css("display", "none");
            $('.basket-pay__text').css("display", "flex");
        });
    }); 

    /*menu-separator*/
    $('.menu-header__delivery ul li input').click(function (e) {
        $('.menu-header__delivery ul li:nth-child(2)').toggleClass('separator');
    });

    /*CALCULATOR*/
    $(function () {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 0 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
    /*CARD PRODUCT SLICK*/
    
    $('.card-row-js').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        appendArrows: $('.next-card-first'),
        prevArrow: '<a href="#" class="card-prev-btn" aria-disabled="true">&larr;</a>',
        nextArrow: '<a href="#" class="card-prev-btn active-link" aria-disabled="false">&rarr;</a>',
        variableWidth: true,
        responsive: [
        {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: 1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });

    $('.card-row-js-2').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        appendArrows: $('.next-card-second'),
        prevArrow: '<a href="#" class="card-prev-btn" aria-disabled="true">&larr;</a>',
        nextArrow: '<a href="#" class="card-prev-btn active-link" aria-disabled="false">&rarr;</a>',
        variableWidth: true,
        responsive: [
        {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: 0,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    initialSlide: 0,
                    variableWidth: false,
                }
            }
        ]
    });

     $('.card-row-js-3').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        appendArrows: $('.cards-row__pagintaion'),
        variableWidth: true,
        prevArrow: '<a href="#" class="cards-row__pagintaion_arrow-left" aria-disabled="true"><span></span></a>',
        nextArrow: '<a href="#" class="cards-row__pagintaion_arrow-right cards-row__pagintaion_arrow-active" aria-disabled="false"><span></span></a>',
        responsive: [
         {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: -1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });

     $('.card-row-js-4').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        arrows: false,
        variableWidth: true,
        responsive: [
         {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: 0,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });

    $('.card-row-js-5').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        arrows: false,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: -1,
                }
            }, {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });

     $('.card-row-js-6').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        arrows: false,
        variableWidth: true,
        responsive: [
         {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: -1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });

    $('.card-row-js-7').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        arrows: false,
        variableWidth: true,
        responsive: [
          {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: -1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });

    $('.card-row-js-8').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        appendArrows: $('.next-card-second'),
        prevArrow: '<a href="#" class="card-prev-btn" aria-disabled="true">&larr;</a>',
        nextArrow: '<a href="#" class="card-prev-btn active-link" aria-disabled="false">&rarr;</a>',
        variableWidth: true,
        responsive: [
        {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: -1,

                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });

    $('.card-row-js-9').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 3,
        arrows : false,
        variableWidth: true,
        responsive: [
        {
                breakpoint: 1023,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    initialSlide: -1,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    vertical: true,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    variableWidth: false,
                }
            }
        ]
    });
    
    /*HOSHI INPUT*/
    $('.input__field').change(function () {
        var $this = $(this);
        var $thisInput = $this.find('input');
        if ($this.val() !== "") {
            $this.addClass('input--filled');
        } else {
            $this.removeClass('input--filled');
        }
    });

    /*MAGIC CARD*/
    $('.main-menu__card').click(function () {
        $(this).addClass('magictime tinRightOut');
    }); 

     /*SLIDER*/
    (function () {
        var $slides = document.querySelectorAll('.slide');
        var $controls = document.querySelectorAll('.slider__control');
        var numOfSlides = $slides.length;
        var slidingAT = 1300; // sync this with scss variable
        var slidingBlocked = false;

        [].slice.call($slides).forEach(function ($el, index) {
            var i = index + 1;
            $el.classList.add('slide-' + i);
            $el.dataset.slide = i;
        });

        [].slice.call($controls).forEach(function ($el) {
            $el.addEventListener('click', controlClickHandler);
        });

        function controlClickHandler() {
            if (slidingBlocked) return;
            slidingBlocked = true;

            var $control = this;
            var isRight = $control.classList.contains('m--right');
            var $curActive = document.querySelector('.slide.s--active');
            var index = +$curActive.dataset.slide;
            (isRight) ? index++ : index--;
            if (index < 1) index = numOfSlides;
            if (index > numOfSlides) index = 1;
            var $newActive = document.querySelector('.slide-' + index);

            $control.classList.add('a--rotation');
            $curActive.classList.remove('s--active', 's--active-prev');
            document.querySelector('.slide.s--prev').classList.remove('s--prev');

            $newActive.classList.add('s--active');
            if (!isRight) $newActive.classList.add('s--active-prev');


            var prevIndex = index - 1;
            if (prevIndex < 1) prevIndex = numOfSlides;

            document.querySelector('.slide-' + prevIndex).classList.add('s--prev');

            setTimeout(function () {
                $control.classList.remove('a--rotation');
                slidingBlocked = false;
            }, slidingAT * 0.75);
        };
    }());

    (function () {
        let allSlides = document.querySelectorAll('.slide');
        let list = document.querySelector('.slider-dots');

        for (let i = 0; i < allSlides.length; i++) {
            var indexSlide = i + 1;
            let li = document.createElement('li');
            li.className = 'li-dot';
            li.dataset.indexNav = indexSlide;
            $('.li-dot').eq(2).addClass('slider-dot__active');
            list.appendChild(li);
            let span = document.createElement('span');
            span.className = 'slider-dot';
            li.appendChild(span);
        };

        (function () {
            var dot = document.querySelectorAll('.li-dot');
            var handler = function () {
                if ($('.li-dot').hasClass('slider-dot__active')) {
                    $('.li-dot').removeClass('slider-dot__active');
                    $(this).addClass('slider-dot__active');
                    var obj = $(this).data('indexNav'); // узнаем его номер
                    var preObj = obj - 1;
                    if ($('.slide').hasClass('s--active')) {
                        $('.slide').removeClass('s--active');
                        $('.slide').removeClass('s--prev');
                        $('.slide[data-slide="' + obj + '"]').addClass('s--active');
                        $('.slide[data-slide="' + preObj + '"]').addClass('s--prev');
                    }

                }
                else {
                    $(this).addClass('slider-dot__active');
                }
                return;
            }
            for (var i = 0; i < dot.length; i++) {
                dot[i].onclick = handler;
            }
        }());
      }());  

    /*HIDE TEXT ...*/
	    if(window.matchMedia('(max-width: 767px)').matches) {
	        $(function(){
	            $('.card-product__name').liTextLength({
	                length: 25,                                 
	                afterLength: '...',                                 
	                fullText:false
	            });
	        });
	    }

	    if(window.matchMedia('(min-width: 768px) and (max-width: 1024px)').matches) {
	    	$(function(){
	            $('.choose-product-basket__text').liTextLength({
	                length: 44,                                 
	                afterLength: '...',                                 
	                fullText:false
	            });
	        });
	    }

     /*VALIDATE*/
    $.validator.messages.required = '';

    $('#basket-address-form').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            basketAddressName: {
                required: true
            },
            basketAddressPhone: {
                required: true
            },
            basketAddressAddress: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            } else {
                $(".errorForm").hide();
                $('.input__label-content').show();
            }
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            submit();
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });

    $('#basket-address-form-tablet').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            basketAddressNameTablet: {
                required: true
            },
            basketAddressPhoneTablet: {
                required: true
            },
            basketAddressAddressTablet: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            } else {
                $(".errorForm").hide();
                $('.input__label-content').show();
            }
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            submit();
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });

    $('#basket-address-form-mobile').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            basketAddressNameMobile: {
                required: true
            },
            basketAddressPhoneMobile: {
                required: true
            },
            basketAddressAddressMobile: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            } else {
                $(".errorForm").hide();
                $('.input__label-content').show();
            }
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            submit();
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });

    $('#login-form').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            mphone: {
                required: true
            },
            mpass: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            } else {
                $(".errorForm").hide();
                $('.input__label-content').show();
            }
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            submit();
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });

    $('#form-login-sms').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            smsphone: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            } 
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            $('.modalblock-login-sms').hide(300);
            $('.modalblock-login-sms-2').show(300);
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });

     $('#form-login-sms-2').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            smsphone2: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            } else {
                $(".errorForm").hide();
                $('.input__label-content').show();
            }
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            $('.modalblock-login-sms-2').hide(300);
            $('.modalblock-login-sms-success').show(300);
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });

      $('#form-registration').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            regphone: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            }
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            $('.modalblock-registration').hide(300);
            $('.modalblock-registration-2').show(300);
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });


     $('#form-registration-2').validate({
        onkeyup: false,
        focusInvalid: false,
        rules: {
            regphone2: {
                required: true
            }
        },
        invalidHandler: function (event, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $(".errorForm").show();
                $('.input__label-content').hide();
            }
            $(".input--hoshi").on('click', function (event) {
                $(this).children().empty();
            });
        },
        submitHandler: function () {
            $('.modalblock-registration-2').hide(300);
            $('.modalblock-registration-success').show(300);
            $(".errorForm").hide();
            $('.input__label-content').show();
        }
    });

    /*modalblock phone maskmn*/
    $(".modalblock-phone").mask("+7 999-999-99-99");
    

    /*modalblock*/
    $('.modal').children().hide();
    
    $('.modal-open-login').click(function(){
        $('.modal-open-registration').removeClass('sign-active');
        $('.modal-open-login').addClass('sign-active');
        $('.modal-basket-ui').hide();
        $('.modal-more').hide();
        $('.modal').show();
        $('.modalblock-login').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
    });
    
    $('.modal-open-registration').click(function () {
        $('.modal-open-login').removeClass('sign-active');
        $('.modal-open-registration').addClass('sign-active');
        $('.modal-basket-ui').hide();
        $('.modal-more').hide();
        $('.modal').show();
        $('.modalblock-registration').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
    });

    $('.modal-add-basket').click(function () {
        $('.modal-basket-ui').hide();
        $('.modal-more').hide();
        $('.modal').show();
        $('.modalblock-add-basket').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
        return false;
    });

    $('.card-product__check').click(function () {
        $('.modal-basket-ui').hide();
        $('.modal').hide();
        $('.modal-more').show();
        $('.modalblock-more').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
        return false;
    });

    $('.open-modal-score').click(function () {
        $('.modal-basket-ui').hide();
        $('.modal-more').hide();
        $('.modal').show();
        $('.modalblock-score').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
        return false;
    });

    $('.open-modal-address').click(function () {
        $('.modal-basket-ui').hide();
        $('.modal-more').hide();
        $('.modal').show();
        $('.modalblock-address').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
    });

    $('.open-modal-change').click(function () {
        $('.modal-basket-ui').hide();
        $('.modal-more').hide();
        $('.modal').show();
        $('.modalblock-change-ingredients').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
        return false;
    });

    $('.open-modal-basket-ui').click(function () {
        $('.modal').hide();
        $('.modal-basket-ui').show();
        $('.modalblock-basket-ui').show(300);
        var buttonId = $(this).attr('data-effect');
        $('#modal-container').removeAttr('class').addClass(buttonId);
        $('body').addClass('modal-active');
        return false;
    });

    $('#modal-container').click(function(e){
        var target = $(event.target);
        var modalBG = $('.modal-background');
        if (target.is(modalBG)) {
            $('#modal-container').addClass('out');
            $('body').removeClass('modal-active');
            $('.modal').children().hide();
        }
    });
    
    $('.no-account').click(function(){
        $('.modalblock-login').hide(300);
        $('.modalblock-registration').show(300);
    });

    $('.forgot-password').click(function () {
        $('.modalblock-login').hide(300);
        $('.modalblock-login-sms').show(300);
    });

    $('.modalblock-close').click(function (e) {
        $('#modal-container').addClass('out');
        $('body').removeClass('modal-active');
        $('.modal').children().hide();
    });
    
   /*yandex map order*/

            ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map("basket-address-map", {
                center: [55.76666, 37.64666],
                zoom: 17,
                controls: []
            }),
                myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                    hintContent: 'Собственный значок метки',
                    balloonContent: 'Это красивая метка'
                }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#image',
                        // Своё изображение иконки метки.
                        iconImageHref: 'img/basket-map.png',
                        // Размеры метки.
                        iconImageSize: [18, 18],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [5, 38]
                    });
            myMap.geoObjects
                .add(myPlacemark);

            var myMap2 = new ymaps.Map("basket-address-map2", {
                center: [55.76666, 37.64666],
                zoom: 17,
                controls: []
            }),     
                myPlacemark2 = new ymaps.Placemark([55.76655, 37.64655], {
                    balloonContent: 'цвет',
                }, {
                        preset: 'islands#yellowCircleDotIconWithCaption',
                        iconCaptionMaxWidth: '50'
                });

            myMap2.geoObjects
                .add(myPlacemark2);

            var btnDeliv = document.querySelector('.basket-pay__button_delivery-desc');
            var btnPickup = document.querySelector('.basket-pay__button_pickup-desc');

            if (window.matchMedia('(min-width: 1024px)').matches) {
                var descMap = document.querySelector('#basket-address-map');
                var descMap2 = document.querySelector('#basket-address-map2');

                ymaps.domEvent.manager.add(btnDeliv, 'click', function () {
                    descMap.style.display = "none";
                    descMap2.style.display = "block";
                });

                ymaps.domEvent.manager.add(btnPickup, 'click', function () {
                    descMap.style.display = "block";
                    descMap2.style.display = "none";
                });
                
            }
                
            var myMapTablet = new ymaps.Map("basket-address-map-tablet", {
                center: [55.76666, 37.64666],
                zoom: 17,
                controls: []
            }),
                myPlacemark = new ymaps.Placemark(myMapTablet.getCenter(), {
                    hintContent: 'Собственный значок метки',
                    balloonContent: 'Это красивая метка'
                }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#imageWithContent',
                        // Своё изображение иконки метки.
                        iconImageHref: 'img/basket-map.png',
                        // Размеры метки.
                        iconImageSize: [18, 18],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [5, 38]
                    });

            myMapTablet.geoObjects
                .add(myPlacemark);
                    
            var myMapTablet2 = new ymaps.Map("basket-address-map-tablet2", {
                center: [55.76666, 37.64666],
                zoom: 17,
                controls: []
            }),     
                myPlacemark2 = new ymaps.Placemark([55.76655, 37.64655], {
                    balloonContent: 'цвет',
                }, {
                        preset: 'islands#yellowCircleDotIconWithCaption',
                        iconCaptionMaxWidth: '50'
                });

            myMapTablet2.geoObjects
                .add(myPlacemark2);

            if (window.matchMedia('(max-width: 1023px)').matches) {
                var btnDeliv2 = document.querySelector('.basket-pay__button_delivery-tab');
                var btnPickup2 = document.querySelector('.basket-pay__button_pickup-tab');

                var tabMap = document.querySelector('#basket-address-map-tablet');
                var tabMap2 = document.querySelector('#basket-address-map-tablet2');

                ymaps.domEvent.manager.add(btnDeliv2, 'click', function () {
                    tabMap.style.display = "none";
                    tabMap2.style.display = "block";
                });

                ymaps.domEvent.manager.add(btnPickup2, 'click', function () {
                    tabMap.style.display = "block";
                    tabMap2.style.display = "none";
                });
            }   
                
                


            var myMapMobile = new ymaps.Map("basket-address-map-mobile", {
                center: [55.76666, 37.64666],
                zoom: 17,
                controls: []
            }),
                myPlacemark = new ymaps.Placemark(myMapMobile.getCenter(), {
                    hintContent: 'Собственный значок метки',
                    balloonContent: 'Это красивая метка'
                }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#imageWithContent',
                        // Своё изображение иконки метки.
                        iconImageHref: 'img/basket-map.png',
                        // Размеры метки.
                        iconImageSize: [18, 18],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [5, 38]
                    });

            myMapMobile.geoObjects
                .add(myPlacemark);
                    
            var myMapMobile2 = new ymaps.Map("basket-address-map-mobile2", {
                center: [55.76666, 37.64666],
                zoom: 17,
                controls: []
            }),     
                myPlacemark2 = new ymaps.Placemark([55.76655, 37.64655], {
                    balloonContent: 'цвет',
                }, {
                        preset: 'islands#yellowCircleDotIconWithCaption',
                        iconCaptionMaxWidth: '50'
                });

                myMapMobile2.geoObjects
                .add(myPlacemark2);

            if (window.matchMedia('(max-width: 767px)').matches) {
                var btnDeliv3 = document.querySelector('.basket-pay__button_delivery-mob');
                var btnPickup3 = document.querySelector('.basket-pay__button_pickup-mob');

                var mobMap = document.querySelector('#basket-address-map-mobile');
                var mobMap2 = document.querySelector('#basket-address-map-mobile2');

                ymaps.domEvent.manager.add(btnDeliv3, 'click', function () {
                    mobMap.style.display = "none";
                    mobMap2.style.display = "block";
                });

                ymaps.domEvent.manager.add(btnPickup3, 'click', function () {
                    mobMap.style.display = "block";
                    mobMap2.style.display = "none";
                });
            }   
        }
    
});